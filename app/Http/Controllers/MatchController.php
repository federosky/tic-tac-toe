<?php
/**
 *
 */
namespace App\Http\Controllers;

use App\Model\Match as MatchModel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log as Logger;
/**
 *
 */
class MatchController extends Controller
{
    const PLAYER_X_ID = 1;
    const PLAYER_O_ID = 2;

    /**
     * App Entry point
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function index()
    {
        return view('index');
    }

    /**
     * Returns a list of matches
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function matches()
    {
        return response()->json($this->getMatches());
    }

    /**
     * Returns the state of a single match
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function match($id)
    {
        try {
            $match = MatchModel::findOrFail($id);
        } catch (\Exception $e) {
            Logger::error(sprintf('[%s] %s', __METHOD__, $e->getMessage()));
            $match = [];
        }

        return response()->json($match);
    }

    /**
     * Makes a move in a match
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function move($id)
    {
        // Retrieve match
        try {
            $match = MatchModel::findOrFail($id);
        } catch (\Exception $e) {
            Logger::error(sprintf('[%s] %s', __METHOD__, $e->getMessage()));
            return response()->json([]);
        }

        $position = Input::get('position');
        $player   = $match->next;
        // Update position on board
        $board = $match->board;
        // Set player value on position
        $board[$position] = $player;
        $match->board = $board;
        // Set next
        $match->next = ($player - ((-1)**$player));
        // Check for Winner
        if ($match->isWinnerMove($position)) {
            $match->winner = $player;
        }

        // Save match
        try {
            $match->saveOrFail();
        } catch (\Exception $e) {
            Logger::error(sprintf('[%s] %s', __METHOD__, $e->getMessage()));
            $match = [];
        }

        return response()->json($match);
    }

    /**
     * Creates a new match and returns the new list of matches
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        // Default values
        $defaults = [
            'name' => 'Match ##',
            // Init with either one of the players, to avoid "not your turn" message.
            'next' => rand(self::PLAYER_X_ID, self::PLAYER_O_ID),
            'winner' => 0,
            'board' => [0, 0, 0, 0, 0, 0, 0, 0, 0]
        ];
        // Prepare model
        $match = new MatchModel($defaults);
        try {
            $match->save();
            $match->name = sprintf('Match%s', $match->id);
            $match->save();
        } catch (\Exception $e) {
            Logger::error(sprintf('[%s] %s', __METHOD__, $e->getMessage()));
        }

        return response()->json($this->getMatches());
    }

    /**
     * Deletes the match and returns the new list of matches
     *
     * @param integer $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        // Delete without retrieving the object
        MatchModel::destroy($id);

        return response()->json($this->getMatches());
    }

    /**
     * Retrieve all matches
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function getMatches()
    {
        return MatchModel::all();
    }

    /**
     * Creates a fake array of matches
     *
     * @return \Illuminate\Support\Collection
     */
    private function fakeMatches() {
        return collect([
            [
                'id' => 1,
                'name' => 'Match1',
                'next' => 2,
                'winner' => 1,
                'board' => [
                    1, 0, 2,
                    0, 1, 2,
                    0, 2, 1,
                ],
            ],
            [
                'id' => 2,
                'name' => 'Match2',
                'next' => 1,
                'winner' => 0,
                'board' => [
                    1, 0, 2,
                    0, 1, 2,
                    0, 0, 0,
                ],
            ],
            [
                'id' => 3,
                'name' => 'Match3',
                'next' => 1,
                'winner' => 0,
                'board' => [
                    1, 0, 2,
                    0, 1, 2,
                    0, 2, 0,
                ],
            ],
            [
                'id' => 4,
                'name' => 'Match4',
                'next' => 2,
                'winner' => 0,
                'board' => [
                    0, 0, 0,
                    0, 0, 0,
                    0, 0, 0,
                ],
            ],
        ]);
    }
}
