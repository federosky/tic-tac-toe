<?php
/**
 *
 */
namespace App\Model;
/**
 *
 */
use Illuminate\Database\Eloquent\Model;
/**
 * Match model
 *  - Wraps entity
 */
class Match extends Model
{
    /**
     * n value for a square board of nxn
     */
    const BOARD_SIZE = 3;

    /**
     * The attributes that should be cast
     *
     * @var array
     */
    protected $casts = [
        'board' => 'array'
    ];

    /**
     * The attrs that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    // Standard behaviour

    /**
     * Determine whether a given move is a game winner one.
     *
     * @param integer $position
     *
     * @return boolean
     */
    public function isWinnerMove($position)
    {
        // Board size
        $size = self::BOARD_SIZE;
        $player = $this->board[$position];
        $checkSum = ($size * $player);
        // Check current column
        $column = ($position % $size);
        for ($i = 0, $sum = 0; $i < $size; $i++) {
            $positionIndex = ($column + ($size * $i));
            if ($this->board[$positionIndex] == $player) {
                $sum += $this->board[$positionIndex];
            }
        }
        if ($sum == $checkSum) {
            return true;
        }
        // Check current row
        $row    = (($position - $column) / $size);
        for ($i = 0, $sum = 0; $i < $size; $i++) {
            $positionIndex = (($size * $row) + $i);
            if ($this->board[$positionIndex] == $player) {
                $sum += $this->board[$positionIndex];
            }
        }
        if ($sum == $checkSum) {
            return true;
        }
        // Check diagonals
        // Check only even positions
        if (($position % 2)) {
            return false;
        }
        for ($i = 0, $sum = 0; $i < $size; $i++) {
            $positionIndex = ($i * ($size + 1));
            if ($this->board[$positionIndex] == $player) {
                $sum += $this->board[$positionIndex];
            }
        }
        if ($sum == $checkSum) {
            return true;
        }
        for ($i = 0, $sum = 0; $i < $size; $i++) {
            $positionIndex = (($size - 1) * (1 + $i));
            if ($this->board[$positionIndex] == $player) {
                $sum += $this->board[$positionIndex];
            }
        }
        if ($sum == $checkSum) {
            return true;
        }

        return false;
    }
}
